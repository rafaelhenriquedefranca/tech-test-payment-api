﻿namespace PaymentAPI.Models
{
    public class PerfilGerenciamento
    {
        public List<Perfil> Usuarios = new List<Perfil>();

        public void AdicionarPerfil(Perfil perfil)
        {
            Usuarios.Add(perfil);
        }
    }
}