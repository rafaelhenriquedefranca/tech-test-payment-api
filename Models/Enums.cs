﻿public enum VendaStatus
{
    AguardandoPagamento,
    PagamentoAprovado,
    EnviadoParaTransportadora,
    Cancelada,
    Entregue
}