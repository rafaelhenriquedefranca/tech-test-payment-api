﻿namespace PaymentAPI.Models
{
    public class VendaGerenciamento
    {
        public List<Produto> Carrinho { get; set; }
        public List<Venda> Vendas = new List<Venda>();


        public VendaGerenciamento()
        {
            Carrinho = new List<Produto>();
        }
        
        public List<Produto> MostrarCarrinho()
        {
            return (Carrinho.ToList());
        }

        public void FecharVenda(Perfil vendedor, List<Produto> produto)
        {
            int id;
            decimal valorFinal = 0;

            if (Vendas.Count() == 0)
                id = 1;
            else
                id = Vendas.Count() + 1;

            foreach(var item in Carrinho)
            {
                valorFinal += item.Quantidade * item.Valor;
            }

            var venda = new Venda(id, vendedor, produto, valorFinal);
            Vendas.Add(venda);
            Carrinho.Clear();
        }

        public string AlterarVenda(Venda compra, string status)
        {
            switch (compra.Status.ToLower())
            {
                case "aguardandopagamento":
                    if (status.ToLower() == "pagamentoaprovado" || status.ToLower() == "cancelada")
                    {
                        compra.Status = status;
                        return compra.Status.ToString();
                    }
                    break;
                
                case "pagamentoaprovado":
                    if (status.ToLower() == "enviadoparatransportadora" || status.ToLower() == "cancelada") 
                    {
                        compra.Status = status;
                        return compra.Status.ToString();
                    }
                    break;

                case "enviadoparatransportador":
                    if (status.ToLower() == "entregue")
                    {
                        compra.Status = status;
                        return compra.Status.ToString();
                    }
                    break;

            }
            return null;
        }
    }
}
