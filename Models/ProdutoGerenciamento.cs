﻿namespace PaymentAPI.Models
{
    public class ProdutoGerenciamento
    {
        public List<Produto> Produtos { get; set; }

        public ProdutoGerenciamento()
        {
            Produtos = new List<Produto>();
        }
    }
}
