﻿namespace PaymentAPI.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public Perfil Vendedor { get; set; }
        public List<Produto> Produtos { get; set; }
        public string Status { get; set; }
        public DateTime DataDeVenda { get; set; }
        public decimal ValorFinal { get; set; }

        public Venda(int id, Perfil vendedor, List<Produto> produtos, decimal valorFinal) 
        {
            Id = id;
            Vendedor = vendedor;
            Produtos = produtos;
            Status = VendaStatus.AguardandoPagamento.ToString();
            DataDeVenda = DateTime.Now.Date;
            ValorFinal = valorFinal;
        }
    }
}
