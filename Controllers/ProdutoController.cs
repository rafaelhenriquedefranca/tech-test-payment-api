﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Models;

namespace PaymentAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        public readonly ProdutoGerenciamento _produto;
        public readonly VendaGerenciamento _carrinho;
        public ProdutoController(ProdutoGerenciamento produto, VendaGerenciamento carrinho) 
        { 
            _produto = produto;
            _carrinho = carrinho;
        }

        [HttpPost("Criar Produto")]
        public IActionResult CriarProduto(Produto produto)
        {
            produto.Id = _produto.Produtos.Count() + 1;
            _produto.Produtos.Add(produto);

            return Ok(produto);
        }

        [HttpGet("Mostrar Todos Produtos")]
        public IActionResult MostrarProdutos()
        {
            return Ok(_produto.Produtos.ToList());
        }

        [HttpPost("Adicionar Produto No Carrinho")]
        public IActionResult AdicionarProdutoNaCompra(int idProduto)
        {
            var produto = _produto.Produtos.Find(x => x.Id == idProduto);

            if (produto == null)
                return NotFound();

            _carrinho.Carrinho.Add(produto);

            return Ok(produto);
        }
    }
}
