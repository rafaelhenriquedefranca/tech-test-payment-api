﻿using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Models;

namespace PaymentAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PerfilController : ControllerBase
    {
        public readonly PerfilGerenciamento _usuarios;
        public PerfilController(PerfilGerenciamento usuarios)
        {
            _usuarios = usuarios;
        }


        [HttpPost]
        [Route("Criar Perfil")]
        public IActionResult CriarPerfil(Perfil perfil)
        {
            perfil.Id = _usuarios.Usuarios.Count() + 1;


            _usuarios.AdicionarPerfil(perfil);
            return Ok(perfil);
        }

        [HttpGet("Buscar Perfil")]
        public IActionResult MostrarPerfils()
        {
            return Ok(_usuarios.Usuarios.ToList());
        }

    }
}
