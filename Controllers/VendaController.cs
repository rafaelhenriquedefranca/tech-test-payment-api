﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Models;

namespace PaymentAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        public readonly VendaGerenciamento _vendaGerenciamento;
        public readonly PerfilGerenciamento _perfilGerenciamento;
        public VendaController(VendaGerenciamento vendaGerenciamento, PerfilGerenciamento perfil) 
        { 
            _vendaGerenciamento = vendaGerenciamento;
            _perfilGerenciamento = perfil;
        }

        [HttpGet("Carrinho")]
        public IActionResult Carrinho()
        {
            return Ok(_vendaGerenciamento.MostrarCarrinho());
        }

        [HttpGet("Fechar Venda")]
        public IActionResult FecharVenda(int idVendedor)
        {
            var produtos = _vendaGerenciamento.Carrinho;
            var perfil = _perfilGerenciamento.Usuarios.Find(x => x.Id == idVendedor);

            if (perfil == null)
                return NotFound();

            _vendaGerenciamento.FecharVenda(perfil, produtos);
            return Ok();
        }

        [HttpGet("Procurar Venda")]
        public IActionResult ProcurarVenda(int id)
        {
            var compra = _vendaGerenciamento.Vendas.Find(x => x.Id == id);

            if (compra == null)
                return NotFound();

            return Ok(compra);
        }

        [HttpPut("Alterar Venda")]
        public IActionResult AlterarVenda(int id, string status)
        {
            var compra = _vendaGerenciamento.Vendas.Find(x => x.Id == id);

            if (compra == null)
                return NotFound();

            var novoStatus = _vendaGerenciamento.AlterarVenda(compra, status);

            if (novoStatus == null) 
                return NotFound(novoStatus);

            compra.Status = novoStatus;

            return Ok(compra);
        }

        [HttpGet("Consulta de Todas Vendas")]
        public IActionResult MostrarPedidos()
        {
            return Ok(_vendaGerenciamento.Vendas.ToList());
            //return Ok(_vendaGerenciamento.MostrarPedidos());
        }
    }
}
